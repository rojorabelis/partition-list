# Partition list

Partition list est une libraire qui partitionne une liste en sous-liste.
### Instructions
  - Dans la racine du projet, lancer la commande `mvn package`
  - Importer le fichier *tp-0.0.1.jar* qui se situe de *target/* dans votre projet
  - Instancier l'interface `com.formation.util.IUtil` par la classe concr�te `com.formation.util.Util`
  - Appeller la m�thode *partition(list,size)* dans `com.formation.util.IUtil`
