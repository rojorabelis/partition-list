package tp;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.formation.util.IUtil;
import com.formation.util.Util;

public class UtilsTest {
	List<Integer> test;
	
	private static IUtil util ;
	@BeforeClass
    public static void initUtil() {
		util = new Util();
    }

	@Before
	public void setUp() {
		test = new ArrayList<>();
		test.add(1);
		test.add(23);
		test.add(34);
		test.add(56);
		test.add(7);
		test.add(91);
	}

	@Test
	public void test() {
		List<List<Integer>> t = util.partition(test, 4);
		int total = test.size();

		int part = total / 4;
		if (total % 4 != 0)
			part++;

		assertEquals(t.size(), part);
	}
}
