package com.formation.util;

import java.util.List;

public interface IUtil {
	/**
	 * Partitionne une liste en sous liste divisé en n taille
	 * @param liste liste initiale
	 * @param taille taille pour chaque sous liste
	 * @return 
	 */
	List<List<Integer>> partition(List<Integer> liste,int taille);
}
