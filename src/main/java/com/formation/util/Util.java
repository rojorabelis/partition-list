package com.formation.util;

import java.util.ArrayList;
import java.util.List;

public class Util implements IUtil {

	@Override
	public List<List<Integer>> partition(List<Integer> liste, int taille) {
		int total = liste.size();

		int part = total / taille;
		if (total % taille != 0)
			part++;

		List<List<Integer>> ret = new ArrayList<>();
		for (int i = 0; i < part; i++) {
			int begin = i * taille;
			int end = (i * taille + taille < total) ? (i * taille + taille) : total;
			List<Integer> temp = new ArrayList<>();
			temp = liste.subList(begin, end);
			ret.add(temp);
		}

		return ret;
	}

}
